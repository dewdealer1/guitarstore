package company;

import java.util.Scanner;

public class GuitarStore {
    private final String exit = "exit";
    private final String add = "add";
    private final String print = "print";
    private final String rebrand = "rebrand";
    private final String help = "help";
    private final String remove = "remove";
    //TODO: when you learn about lists, change the commands to be a list of commands, update the help method accordingly
    private String storeName = "Lame Strings";
    //TODO: when you learn about lists, change guitars to be a list, update commands accordingly
    private Guitar guitar0;
    private Guitar guitar1;
    private Guitar guitar2;
    private Guitar guitar3;

    public void run() {
        print("Welcome to " + storeName + ", how can I help you?");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String command = scanner.next();

            if (exit.equals(command)) {
                break;
            } else if (help.equals(command)) {
                print("I am currently able to accept the following commands:" + exit + ", " + help + ", " + rebrand +
                        ", " + add + ", " + print);
            } else if (rebrand.equals(command)) {
                print("What would you like to call your store?");
                scanner.nextLine();
                storeName = scanner.nextLine();
                print("Cool, welcome to " + storeName);
            } else if (add.equals(command)) {
                print("Brand please");
                String brand = scanner.next();
                print("How many strings?");
                int numStrings = scanner.nextInt();
                print("Is the pickup passive?");
                boolean isPassive = scanner.nextBoolean();
                if (guitar0 == null) {
                    guitar0 = new Guitar(brand, numStrings, isPassive);
                } else if (guitar1 == null) {
                    guitar1 = new Guitar(brand, numStrings, isPassive);
                } else if (guitar2 == null) {
                    guitar2 = new Guitar(brand, numStrings, isPassive);
                } else if (guitar3 == null) {
                    guitar3 = new Guitar(brand, numStrings, isPassive);
                }
            } else if (print.equals(command)) {
                //TODO: this prints out kind of weird, try overriding the toString method in the guitar class (use the alt+insert dialog and choose toString
                if (guitar0 != null) {
                    print(guitar0.toString());
                }
                if (guitar1 != null) {
                    print(guitar1.toString());
                }
                if (guitar2 != null) {
                    print(guitar2.toString());
                }
                if (guitar3 != null) {
                    print(guitar3.toString());
                }
            } else if (remove.equals(command)) {
                //TODO: Implement remove command
                print("Sorry can't help you there yet, maybe soon though");
            }
            //TODO: Create a new update command that changes an existing guitar
            print("Anything else I can help you with?");
        }
        print("Thank you, please come again");
        //BIG STUFF
        //TODO: we'd really like to start carrying some other inventory, create a new class and allow for it to be managed
        //TODO: we need to be able to sell stuff, maybe we should put prices on inventory and add a sale command
        //TODO: if we're able to sell stuff, we should keep track of how much money the store makes
        //TODO: turns out stuff costs money, we should track how much we buy vs how much we sell to see net gain
        //TODO: maybe we should update our inventory so that we can carry more than one of each instrument and print out our inventory
        //TODO: kinda sucks that all the info goes away every time, we really should get a database or something
        //TODO: kinda sucks to use the command line, too bad there's not a super spiffy frontend or something
    }

    private void print(String s) {
        System.out.println(s);
    }
}
