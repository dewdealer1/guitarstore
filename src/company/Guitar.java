package company;

public class Guitar {
    private String brand;
    private int numStrings;
    private boolean isPassivePickup;

    public Guitar(String brand, int numStrings, boolean isPassivePickup) {
        this.brand = brand;
        this.numStrings = numStrings;
        this.isPassivePickup = isPassivePickup;
    }
}
